#!/bin/bash

# Begin & end parts of prompt
GIT_PROMPT_START="${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[0m\]"
GIT_PROMPT_END="\[\033[00m\]\$ "

function update_git_status() {
    # Regular Colors
    local COLOR_RED="\[\033[0;31m\]"
    local COLOR_YELLOW="\[\033[0;33m\]"
    local COLOR_BLUE="\[\033[0;34m\]"
    local COLOR_WHITE="\[\033[0;37m\]"
    # Bold
    local COLOR_GREEN_BOLD="\[\033[1;32m\]"
    # High Intensty
    local COLOR_BLACK_BRIGHT="\[\033[1;30m\]"
    # Bold High Intensty
    local COLOR_MAGENTA="\[\033[1;35m\]"

    local COLOR_RESET="\[\033[0m\]"

    # Values for the appearance of prompt for git info
    local GIT_PROMPT_PREFIX="["
    local GIT_PROMPT_SUFFIX="]"
    local GIT_PROMPT_SEPARATOR="|"
    local GIT_PROMPT_BRANCH="${COLOR_MAGENTA}"
    local GIT_PROMPT_AHEAD="↑·"
    local GIT_PROMPT_BEHIND="↓·"
    local GIT_PROMPT_REMOTE=" "
    local GIT_PROMPT_STAGED="${COLOR_YELLOW}●"
    local GIT_PROMPT_CONFLICTS="${COLOR_RED}✖"
    local GIT_PROMPT_CHANGED="${COLOR_BLUE}✚"
    local GIT_PROMPT_UNTRACKED="${COLOR_WHITE}…"
    local GIT_PROMPT_CLEAN="${COLOR_GREEN_BOLD}✔"

    GIT_PROMPT_STATUS=""

    if update_current_git_vars; then
        GIT_PROMPT_STATUS="${GIT_PROMPT_PREFIX}"

        GIT_PROMPT_STATUS="${GIT_PROMPT_STATUS}${GIT_PROMPT_BRANCH}${GIT_BRANCH}${COLOR_RESET}"

        GIT_REMOTE=""
        if [[ $GIT_BEHIND -ne 0 ]]; then
            GIT_REMOTE="${GIT_REMOTE}${GIT_PROMPT_BEHIND}${GIT_BEHIND}${COLOR_RESET}"
        fi
        if [[ $GIT_AHEAD -ne 0 ]]; then
            GIT_REMOTE="${GIT_REMOTE}${GIT_PROMPT_AHEAD}${GIT_AHEAD}${COLOR_RESET}"
        fi
        if [[ "$GIT_REMOTE" != "" ]]; then
            GIT_PROMPT_STATUS="${GIT_PROMPT_STATUS}${GIT_PROMPT_REMOTE}${GIT_REMOTE}${COLOR_RESET}"
        fi
        GIT_PROMPT_STATUS="${GIT_PROMPT_STATUS}${GIT_PROMPT_SEPARATOR}"

        if [[ "$GIT_CLEAN" -eq "1" ]]; then
            GIT_PROMPT_STATUS="${GIT_PROMPT_STATUS}${GIT_PROMPT_CLEAN}${COLOR_RESET}"
        else
            if [[ $GIT_STAGED -ne 0 ]]; then
                GIT_PROMPT_STATUS="${GIT_PROMPT_STATUS}${GIT_PROMPT_STAGED}${GIT_STAGED}${COLOR_RESET}"
            fi
            if [[ $GIT_CONFLICTS -ne 0 ]]; then
                GIT_PROMPT_STATUS="${GIT_PROMPT_STATUS}${GIT_PROMPT_CONFLICTS}${GIT_CONFLICTS}${COLOR_RESET}"
            fi
            if [[ $GIT_CHANGED -ne 0 ]]; then
                GIT_PROMPT_STATUS="${GIT_PROMPT_STATUS}${GIT_PROMPT_CHANGED}${GIT_CHANGED}${COLOR_RESET}"
            fi
            if [[ $GIT_UNTRACKED -ne 0 ]]; then
                GIT_PROMPT_STATUS="${GIT_PROMPT_STATUS}${GIT_PROMPT_UNTRACKED}${GIT_UNTRACKED}${COLOR_RESET}"
            fi
        fi

        GIT_PROMPT_STATUS="${GIT_PROMPT_STATUS}${GIT_PROMPT_SUFFIX}"
    fi

    unset GIT_AHEAD GIT_BEHIND GIT_BRANCH GIT_CHANGED GIT_CLEAN GIT_CONFLICTS GIT_MERGE GIT_REMOTE GIT_STAGED GIT_UNTRACKED
}

function update_current_git_vars() {
    local  GIT_LIST_REV GIT_LIST_STAGED GIT_REMOTE_REF ERR_CODE

    GIT_BRANCH=$(git symbolic-ref -q HEAD 2> /dev/null)
    ERR_CODE=$?
    if [[ $ERR_CODE == 128 ]]; then
        return 1
    fi
    GIT_BRANCH=${GIT_BRANCH/refs\/heads\//}

    GIT_CHANGED=$(git diff --name-status | grep -v "^U"  | wc -l)

    GIT_LIST_STAGED=$(git diff --cached --name-status)

    GIT_STAGED=$(echo -n "$GIT_LIST_STAGED" | grep -v "^U"  | wc -l)

    GIT_CONFLICTS=$(echo -n "$GIT_LIST_STAGED" | grep "^U"  | wc -l)

    GIT_UNTRACKED=$(git ls-files --others --exclude-standard | wc -l)

    if [[ $GIT_STAGED -eq 0 && $GIT_CONFLICTS -eq 0 && $GIT_CHANGED -eq 0 && $GIT_UNTRACKED -eq 0 ]]; then
        GIT_CLEAN=1
    else
        GIT_CLEAN=0
    fi

#    echo $GIT_BRANCH $GIT_STAGED $GIT_CONFLICTS $GIT_CHANGED $GIT_UNTRACKED $GIT_CLEAN

    if [[ -z $GIT_BRANCH ]]; then
        GIT_BRANCH=":"$(git rev-parse --short HEAD)
    else
        GIT_REMOTE=$(git config  branch.${GIT_BRANCH}.remote)
        if [[ -n $GIT_REMOTE ]]; then
            GIT_MERGE=$(git config branch.${GIT_BRANCH}.merge)
        else
            GIT_REMOTE="origin"
            GIT_MERGE="refs/heads/${GIT_BRANCH}"
        fi

        if [[ "$GIT_REMOTE" == '.' ]]; then
            GIT_REMOTE_REF=$GIT_MERGE
        else
            GIT_REMOTE_REF="refs/remotes/${GIT_REMOTE}/${GIT_MERGE:11}"
        fi
    fi

#    echo $GIT_REMOTE $GIT_MERGE $GIT_REMOTE_REF

    GIT_LIST_REV=$(git rev-list --left-right ${GIT_REMOTE_REF}...HEAD 2> /dev/null)
    ERR_CODE=$?
    if [[ $ERR_CODE == 128 ]]; then
        GIT_LIST_REV=$(git rev-list --left-right ${GIT_MERGE}...HEAD 2> /dev/null)
    fi

    GIT_AHEAD=$(echo -n "$GIT_LIST_REV" | grep "^>" | wc -l)
    GIT_BEHIND=$(echo -n "$GIT_LIST_REV" | grep -v "^>" | wc -l)

    return 0
}

# Determine active Python virtualenv details.
function update_python_virtualenv () {
    if [[ -z "$VIRTUAL_ENV" ]]; then
        PYTHON_VIRTUALENV=""
    else
        PYTHON_VIRTUALENV="${COLOR_BLUE}($(basename \"$VIRTUAL_ENV\"))${COLOR_RESET} "
    fi
}

function set_git_prompt() {
    update_python_virtualenv
    update_git_status

    PS1="${GIT_PROMPT_START}${PYTHON_VIRTUALENV}${GIT_PROMPT_STATUS}${GIT_PROMPT_END}"
}

PROMPT_COMMAND=set_git_prompt
