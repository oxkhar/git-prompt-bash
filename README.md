# git prompt for bash write in bash

This prompt is a idea from ["Informative git prompt for bash"](https://github.com/magicmonty/bash-git-prompt)

A **bash** prompt that displays information about the current git repository.
In particular the branch name, difference with remote branch, number of files staged, changed, etc.

Goal is that all process is one bash file where git information is provided with shell sentences.

##  Prompt Structure

By default, the general appearance of the prompt is::

    [<branch> <branch tracking>|<local status>]

The symbols are as follows:

- Local status symbols
    - ``✔``: repository clean
    - ``●n``: there are ``n`` staged files
    - ``✖n``: there are ``n`` unmerged files
    - ``✚n``: there are ``n`` changed but *unstaged* files
    - ``…n``: there are ``n`` untracked files
- Branch tracking symbols
    - ``↑n``: ahead of remote by ``n`` commits
    - ``↓n``: behind remote by ``n`` commits
    - ``↓m↑n``: branches diverged, other by ``m`` commits, yours by ``n`` commits
- Branch symbol: When the branch name starts with a colon ``:``, it means it's actually a hash, not a branch (although it should be pretty clear, unless you name your branches like hashes :) )

## Examples

The prompt may look like the following:

 * ``(master↑3|✚1)``: on branch ``master``, ahead of remote by 3 commits, 1 file changed but not staged
 * ``(status|●2)``: on branch ``status``, 2 files staged
 * ``(master|✚7…2)``: on branch ``master``, 7 files changed, some files untracked
 * ``(master|✖2✚3)``: on branch ``master``, 2 conflicts, 3 files changed
 * ``(experimental↓2↑3|✔)``: on branch ``experimental``; your branch has diverged by 3 commits, remote by 2 commits; the repository is otherwise clean
 * ``(:70c2952|✔)``: not on any branch; parent commit has hash ``70c2952``; the repository is otherwise clean

## Install

 * Clone this repository to your homedir, e.g.

```git clone https://bitbucket.org/oxkhar/gitprompt-bash.git .bash-git-prompt```

 * Source the file ```gitprompt.sh``` from your ```~/.bashrc``` (or ```~/.bash_aliases```) file
 * Go in a git repository and test it!

## Config

At the beginning of file you have colors and prompt configuration variables...

 * **GIT_PROMPT_START** and **GIT_PROMPT_END** to tweak your prompt
 * Colors and literals of repository status start like **GIT_PROMPT_***

